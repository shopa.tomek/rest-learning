import express from "express";
import bodyParser from "body-parser";
import products from './data/products.js'


const app = express();
app.use(bodyParser.json());
app.use('/', products);
const port = 5000;
app.listen(port, ()=> console.log(`link: http://localhost:${port}`));

app.get('/',(req, res)=> {
  res.send("first rout sent");
});


app.get('/',(req, res)=> {
  res.json(products);
});

