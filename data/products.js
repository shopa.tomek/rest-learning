import express, { Router } from "express";
import {v4 as uuidV4} from "uuid";
const router = express.Router()

let products = [
  {
  "id": uuidV4(),
  "name": "Airpods Wireless Bluetooth Headphones",
  "description": "Bluetooth technology lets you connect it with compatible devices wirelessly High-quality AAC audio offers immersive listening experience Built-in microphone allows you to take calls while working",
  "price": 89.99
  },
  {
  "id": uuidV4(),
  "name": "iPhone 11 Pro 256GB Memory",
  "description": "Introducing the iPhone 11 Pro. A transformative triple-camera system that adds tons of capability without complexity. An unprecedented leap in battery life",
  "price": 599.99
  },
  {
  "id": uuidV4(),
  "name": "Cannon EOS 80D DSLR Camera",
  "description": "Characterized by versatile imaging specs, the Canon EOS 80D further clarifies itself using a pair of robust focusing systems and an intuitive design",
  "price": 929.99
  },
  {
  "id": uuidV4(),
  "name": "Sony Playstation 4 Pro White Version",
  "description": "The ultimate home entertainment center starts with PlayStation. Whether you are into gaming, HD movies, television, music",
  "price": 399.99
  },
  {
  "id": uuidV4(),
  "name": "Logitech G-Series Gaming Mouse",
  "description": "Get a better handle on your games with this Logitech LIGHTSYNC gaming mouse. The six programmable buttons allow customization for a smooth playing experience",
  "price": 49.99
  }
  ];


router.get('/products', (req, res)=> {
  res.json(products)
});

router.post('/products', (req, res)=>{
  const { id, name, description, price } = req.body
  products.push({
    id: uuidV4,
    name: name, 
    description: description,
    price: price
  })
  res.json(products);
})

// get route by ID

router.get('/products/:id', (req, res)=> {
  const productId = req.params.id;
  const product = products.find((product)=> {
      return product.id === productId;
  })
  res.json(product);
});

// get route by ID 1 and description

// router.get('/products/:id', (req, res)=>{
//   const productId = req.params.id;

//   const product = products.find((product)=> {
//       return product.id === productId;
//   })
//   res.json(product.description);
// })

//del product by ID

router.delete('/products/:id', (req, res)=>{
  const productId = req.params.id;
  products = products.filter((product)=>{
    return product.id !== productId
  })
  res.json(products);
});

// /products/:id provided as a parameter

router.put('/products/:id', (req, res)=>{
  const productId = req.params.id;
  const {name, description, price} = req.body;

  products = products.map((product)=>{
    if(product.id === productId){
      return {
        id: product.id,
        name, 
        description, 
        price,        
      }
    }
    return product
  })
  res.json(products);
});

export default router;


 